## 检出此项目的步骤
* 配置ssh的key
	* git config --global user.name "陈子云"
	* git config --global user.email "472206618@qq.com"
	* ssh-keygen -t rsa -C "472206618@qq.com"
	* cat ~/.ssh/id_rsa.pub
	* 在[gitlab的设置](https://gitlab.com/profile/keys)里添加上此key
	* 测试连接，`ssh -v git@gitlab.com`，若是成功则设置完毕。

* 检出
	* 检出项目，`git clone git@gitlab.com:everlose/myNote.git`
	* cd myNote

* 修改与提交，例如在修改过README.md后
	* git add README.md
	* git commit -m 'chentt'
	* git push -u origin master

